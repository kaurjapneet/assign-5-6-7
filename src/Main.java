import java.util.*;

public class Main {
 
	public static void main (String[] args) {
		
		Bill hydro = new Hydro(1,"Wednesday,19 June,2019","Hydro",45.35,"HydroOne",29);
		Bill internet = new Internet(2,"Wednesday,19 June,2019","Internet",56.50,"Rogers",500);
		Bill mobile = new Mobile(3,"Thursday,24 January,2019","Mobile",250.69,"Galaxy Samsung Inc.","Prepaid Talk + Text plan",112367890,5,356);
	    Bill mobile1 = new Mobile(4,"Wednesday,19 June,2019","Mobile",300.78," Apple Inc. iPhone X MAX+"," LTE+3G 9.5GB Promo plan",190145678,4,230);
	    
	    ArrayList<Bill> biilList = new ArrayList<>();
	    biilList.add(hydro);
	    biilList.add(internet);
	    Customer customer1  = new Customer(1,"Peter","Sigurdson","Peter Sigurdson","peter@gmail.com", biilList);
	    customer1.setBilllist(biilList);
	    
	    ArrayList<Bill> biilList1 = new ArrayList<>();
	    biilList1.add(hydro);
	    biilList1.add(internet);
	    biilList1.add(mobile);
	    biilList1.add(mobile1);
	    Customer customer2  = new Customer(2,"Emad","Nasrallah","Emad Nasrallah","dr.emad@gmail.com", biilList1);
	    customer2.setBilllist(biilList1);
	    
	    ArrayList<Bill> biilList2 = new ArrayList<>();
	    Customer customer3  = new Customer(3,"Mohammad","Kiani","Mohammad Kiani","mkiani@gmail.com", biilList2);
	    customer3.setBilllist(biilList2);
	    
	    CustomerList customerlist = new CustomerList();
	    customerlist.addCustomer(customer1);
		customerlist.addCustomer(customer2);
		customerlist.addCustomer(customer3);
		customerlist.display(); 
	
	}
}
