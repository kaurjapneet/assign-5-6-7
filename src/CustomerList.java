import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class CustomerList implements IDisplay {

	private ArrayList<Customer> CustomerList = new ArrayList<Customer>();

	//Getter and setters for ArrayList
	public ArrayList<Customer> getCustomerList() {
		return CustomerList;
	}

	public void setCustomerList(ArrayList<Customer> customerList) {
		CustomerList = customerList;
	}

	public void addCustomer(Customer c1)
	{
	CustomerList.add(c1);
	}
	
	@Override
	public void display() {
		Collections.sort(this.CustomerList);
		for (int i = 0; i < CustomerList.size(); i++) {
			CustomerList.get(i).display();
		}
	}
	
}
