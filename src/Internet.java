import java.util.Date;

public class Internet extends Bill {

	private String ProviderName;
	private double InternetGBUsed;
	
	//Constructors
	public Internet(int BillId,String BillDate,String BillType,double TotalBillAmount,String ProviderName,double InternetGBUsed) {
		super(BillId, BillDate, BillType,TotalBillAmount);
		this.ProviderName = ProviderName;
		this.InternetGBUsed = InternetGBUsed;
    }

	//getters and setters
	public String getProviderName() {
		return ProviderName;
	}

	public void setProviderName(String providerName) {
		ProviderName = providerName;
	}

	public double getInternetGBUsed() {
		return InternetGBUsed;
	}

	public void setInternetGBUsed(double internetGBUsed) {
		InternetGBUsed = InternetGBUsed;
	} 
	
	public void display()
	{
		super.display();
		System.out.println("Provider Name: "+ProviderName);
		System.out.println("Internet Usage: "+InternetGBUsed+" GB");
		System.out.println("**************************************************");
	}
}   