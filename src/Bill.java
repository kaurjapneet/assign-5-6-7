import java.util.Date;

public class Bill implements IDisplay, Comparable<Bill> {

   private int BillId;
   private String BillDate;
   private String BillType;
   private Double TotalBillAmount;
	
	//Constructors
	public Bill(int BillId,String BillDate,String BillType,Double TotalBillAmount) {
	       this.BillId = BillId;
	       this.BillDate = BillDate;
	       this.BillType = BillType;
	       this.TotalBillAmount = TotalBillAmount;
	}

	//Getters and Setters
	public int getBillId() {
		return BillId;
	}

	public void setBillId(int billId) {
		BillId = billId;
	}

	public String getBillDate() {
		return BillDate;
	}

	public void setBillDate(String billDate) {
		BillDate = billDate;
	}

	public String getBillType() {
		return BillType;
	}

	public void setBillType(String billType) {
		BillType = billType;
	}

	public Double getTotalBillAmount() {
		return TotalBillAmount;
	}

	public void setTotalBillAmount(Double totalBillAmount) {
		TotalBillAmount = totalBillAmount;
	}

	public void display() {
		// TODO Auto-generated method stub

		System.out.println("Bill Id: "+BillId);
		System.out.println("Bill Date: "+BillDate);
		System.out.println("Bill Type: "+BillType);
		System.out.println("Bill Amount: $"+TotalBillAmount);

	}

	@Override
	public int compareTo(Bill bill2) {
		// TODO Auto-generated method stub
		return this.getTotalBillAmount().compareTo(bill2.getTotalBillAmount());
	}
  }
