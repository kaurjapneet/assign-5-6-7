import java.util.Date;

public class Hydro extends Bill {

	private String AgencyName;
	private int UnitConsumed;
	
	//Constructors
	public Hydro(int BillId,String BillDate,String BillType,double TotalBillAmount,String AgencyName,int UnitConsumed){
		super(BillId,BillDate,BillType,TotalBillAmount);
		this.AgencyName = AgencyName;
		this.UnitConsumed = UnitConsumed;
	}

	//Getters and Setters
	public String getAgencyName() {
		return AgencyName;
	}

	public void setAgencyName(String agencyName) {
		AgencyName = agencyName;
	}

	public int getUnitConsumed() {
		return UnitConsumed;
	}

	public void setUnitConsumed(int unitConsumed) {
		UnitConsumed = unitConsumed;
	}
	
	public void display()
	{
		super.display();
        System.out.println("Comapany Name: "+AgencyName);
        System.out.println("Unit Consumed: "+UnitConsumed+" Units");
        System.out.println("*************************************************");
	}
	
}
