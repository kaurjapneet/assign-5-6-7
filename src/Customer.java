
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;


public class Customer implements IDisplay, Comparable<Customer> {

	private Integer CustomerId;
	private String FirstName;
	private String LastName;
	private String FullName;
	private String EmailId;
	private int BillList;
	ArrayList<Bill> Billlist = new ArrayList<Bill>();
	public void addBill(Bill bill)
	{
    Billlist.add(bill);
	}
	public void sortBill() {
		Collections.sort(this.Billlist);
	}
	
	
	//Constructors
	public Customer(Integer CustomerId,String FirstName,String LastName, String FullName, String EmailId, ArrayList BillList) {
		   this.CustomerId = CustomerId;
		   this.FirstName = FirstName;
		   this.LastName = LastName;
		   this.FullName = FullName;
		   this.EmailId = EmailId;
		   this.Billlist = BillList;
	}

	//Getters and Setters
	public Integer getCustomerId() {
		return CustomerId;
	}

	public void setCustomerId(Integer customerId) {
		CustomerId = customerId;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getFullName() {
		return FullName;
	}

	public void setFullName(String fullName) {
		FullName = fullName;
	}

	public String getEmailId() {
		return EmailId;
	}

	public void setEmailId(String emailId) {
		EmailId = emailId;
	}

	public ArrayList<Bill> getBilllist() {
		return Billlist;
	}
	public void setBilllist(ArrayList<Bill> billlist) {
		Billlist = billlist;
	}

	@Override
	public void display() {
		// TODO Auto-generated method stub
		System.out.println("Customer Id :" + CustomerId);
		System.out.println("Customer FullName :" + FullName);
		System.out.println("Customer Email ID :" + EmailId);
		if(Billlist.size() == 0) {
			System.out.println("~~~NOTE : This Customer has no bills");
		}
		else {
		System.out.println("-----Bill Information-----");
		System.out.println("**************************************************");
		// TODO Rest
		double total = 0;
		for (int i = 0; i < Billlist.size(); i++) {
			total += Billlist.get(i).getTotalBillAmount();
			Billlist.get(i).display();
		}
		
	    System.out.println("Total Bill amount to pay "+"$"+total);
		System.out.println("**************************************************");
	   }
	}
	@Override
	public int compareTo(Customer custmr) {
        if(this.getCustomerId() < custmr.getCustomerId()) {
        	return -1;
        } else if(this.getCustomerId() > custmr.getCustomerId()) {
        	return 1;
        } else {
        	return 0;
        }
    }
}

