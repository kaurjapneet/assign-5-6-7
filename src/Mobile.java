import java.util.Date;

public class Mobile extends Bill {

	private String  MobManufacturerName;
	private String PlanName;
	private int MobNumber;
	private double IntGbUsed;
	private double MinUsed;
	
	//Constructors
	public Mobile(int BillId,String BillDate,String BillType,double TotalBillAmount,String MobManufacturerName,String PlanName,int MobNumber,double IntGbUsed,double MinUsed) {
		super(BillId,BillDate,BillType,TotalBillAmount);
		this.MobManufacturerName = MobManufacturerName;
		this.PlanName = PlanName;
		this.MobNumber = MobNumber;
		this.IntGbUsed = IntGbUsed;
		this.MinUsed = MinUsed;
	}

	//Getters and Setters
	public String getMobManufacturerName() {
		return MobManufacturerName;
	}

	public void setMobManufacturerName(String mobManufacturerName) {
		MobManufacturerName = mobManufacturerName;
	}

	public String getPlanName() {
		return PlanName;
	}

	public void setPlanName(String planName) {
		PlanName = planName;
	}

	public int getMobNumber() {
		return MobNumber;
	}

	public void setMobNumber(int mobNumber) {
		MobNumber = mobNumber;
	}

	public double getIntGbUsed() {
		return IntGbUsed;
	}

	public void setIntGbUsed(int intGbUsed) {
		IntGbUsed = intGbUsed;
	}

	public double getMinUsed() {
		return MinUsed;
	}

	public void setMinUsed(double minUsed) {
		MinUsed = minUsed;
	}
	
	public void display() {
		super.display();
		System.out.println("Manufacturer Name: "+MobManufacturerName);
		System.out.println("Plan Name: "+PlanName);
		System.out.println("Mobile Number: "+MobNumber);
		System.out.println("Internet Usage: "+IntGbUsed+" GB");
		System.out.println("Minute Usage: "+MinUsed+" minutes");
		System.out.println("*********************************************");
	}
	
}
